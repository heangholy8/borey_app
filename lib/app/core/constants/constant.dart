// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class DurationConstant {
  static const d200ms = Duration(milliseconds: 200);
  static const d300ms = Duration(milliseconds: 200);
  static const d500ms = Duration(milliseconds: 500);
  static const d1000ms = Duration(seconds: 1);
  static const d2000ms = Duration(seconds: 2);
  static const d3000ms = Duration(seconds: 3);
  static const d4000ms = Duration(seconds: 4);
  static const d5000ms = Duration(seconds: 5);
}

class Geometry {
  static const tableContentPadding = EdgeInsets.all(22);
  static const tableContentSimple = EdgeInsets.symmetric(
    horizontal: 30,
    vertical: 20,
  );
}

class StyleConstant {
  static BoxDecoration boxDecoration = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(8),
  );
}

class ConfigConstant {
  /// ```
  /// Margin 0 - 6
  /// ```
  static const double margin0 = 4.0;
  static const double margin1 = 8.0;
  static const double margin2 = 16.0;
  static const double margin3 = 18.0;
  static const double margin4 = 24;
  static const double margin5 = 36;
  static const double margin6 = 40;

  ///```
  /// Icon size 1 - 4
  /// ```
  static const double iconSize1 = 16.0;
  static const double iconSize2 = 24.0;
  static const double iconSize3 = 32.0;
  static const double iconSize4 = 48.0;
  static const double iconSize5 = 64.0;

  ///```
  ///objectHeight 1 - 7
  ///```
  static const double objectHeight1 = 48.0;
  static const double objectHeight2 = 56.0;
  static const double objectHeight3 = 64.0;
  static const double objectHeight4 = 72.0;
  static const double objectHeight5 = 96.0;
  static const double objectHeight6 = 120.0;
  static const double objectHeight7 = 240.0;

  /// ```
  /// radius 1 - 4;
  /// ```
  static const double radius1 = 4.0;
  static const double radius2 = 8.0;
  static const double radius3 = 16.0;
  static const double radius4 = 24.0;

  /// ```
  /// circlarRadius1 = BorderRadius.circular(4.0)
  /// ```
  static final BorderRadius circlarRadius1 = BorderRadius.circular(4.0);
  static final BorderRadius circlarRadius2 = BorderRadius.circular(10.0);

  /// ```
  /// circlarRadiusTop1 = BorderRadius.vertical(
  ///   top: Radius.circular(4.0),
  /// );
  /// ```
  // ignore: prefer_const_constructors
  static final BorderRadius circlarRadiusTop1 = BorderRadius.vertical(
    top: const Radius.circular(4.0),
  );

  /// ```
  /// circlarRadiusTop2 = BorderRadius.vertical(
  ///   top: Radius.circular(10.0),
  /// );
  /// ```

  static final BorderRadius circlarRadiusTop2 = BorderRadius.vertical(
    top: Radius.circular(10.0),
  );

  /// ```
  /// circlarRadiusBottom1 = BorderRadius.vertical(
  ///   top: Radius.circular(4.0),
  /// );
  /// ```
  static final BorderRadius circlarRadiusBottom1 = BorderRadius.vertical(
    top: Radius.circular(4.0),
  );

  /// ```
  /// circlarRadiusBottom2 = BorderRadius.vertical(
  ///   top: Radius.circular(10.0),
  /// );
  /// ```
  static final BorderRadius circlarRadiusBottom2 = BorderRadius.vertical(
    bottom: Radius.circular(10.0),
  );

  /// ```
  /// fadeDuration = const Duration(milliseconds: 250)
  /// ```
  static const Duration fadeDuration = Duration(milliseconds: 250);

  /// ```
  /// duration = const Duration(milliseconds: 350)
  /// ```
  static const Duration duration = Duration(milliseconds: 350);

  /// ```
  /// layoutPadding = const EdgeInsets.symmetric(
  ///   horizontal: margin2,
  ///   vertical: margin1,
  /// );
  /// ```
  static const EdgeInsets layoutPadding = EdgeInsets.symmetric(
    horizontal: margin2,
    vertical: margin1,
  );

  static final Border border = Border.all(
    width: 1,
    color: const Color(0xFFCECECE),
  );

  static final List<BoxShadow> boxShadows0 = [
    BoxShadow(
      blurRadius: 14,
      offset: const Offset(2.0, 0),
      color: const Color(0xFF7C7C7C).withOpacity(0.4),
    ),
  ];
}
