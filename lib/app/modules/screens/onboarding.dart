import 'dart:async';
import 'package:borey_app/app/modules/screens/loginPage.dart';
import 'package:borey_app/models/view/onboarding_view.dart';
import 'package:borey_app/repository/onboarding_repo.dart';
import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {

  PageController? _pageController;
  StreamController<int>? _pageIndicatorController;
  String titlebutton='NEXT';

  final _duration = const Duration(milliseconds: 500);
  int currentIndexPage =0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(statusBarBrightness: Brightness.light));
    _pageController = PageController(initialPage: 0);
    _pageIndicatorController = StreamController<int>.broadcast();
    
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController?.dispose();
    _pageIndicatorController?.close();
    super.dispose();
  }
  void _navigateToLogin() async{
    final prefs = await SharedPreferences.getInstance();
    final result = await prefs.setBool('onBoarding-done', true);
    if(result){
      Navigator.of(context).pushReplacement(
        PageRouteBuilder(
          pageBuilder: (ctx, anim1, anim2){
            return const LoginPage();
          },
          transitionsBuilder: (ctx, anim1, anim2, child){
            return FadeTransition(
              opacity: anim1,
              child: child
            );
          }
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Stack(
        children: [
          Positioned(
            top: 76.0, left: 25.0,right: 0.0, 
              child: Row(
                children: [
                  Hero(
                    tag: 'LOGO',
                    child: Image.asset('assets/png/logo_borey.png', width: 50.0, height: 50.0,fit: BoxFit.contain,)
                  ),
                ],
              )
          ),
          Positioned(
            top: 0.0, left: 0.0, right: 0.0, bottom: 0,
            child: PageView(
              //physics:const NeverScrollableScrollPhysics(),
              controller: _pageController,
              onPageChanged: (pageIndex){
                setState(() {
                  currentIndexPage=pageIndex;
                  currentIndexPage==2?titlebutton='GET START':titlebutton='NEXT';
                });
                _pageIndicatorController?.sink.add(pageIndex);
              },
              children: OnBoardingRepository.all.map((e){
                return OnBoardingView(data: e);
              }).toList()
            )
          ),
          Positioned(
            bottom: 46.0, left: 0.0,right: 0.0,
            child: TextButton(
              onPressed: _navigateToLogin,
              child: currentIndexPage==2?Container():const Text('SKIP'),
              style: TextButton.styleFrom(primary: TextColor.black87),
            )
          ),
          Positioned(
            bottom: 216.0, right: 16.0, left: 16.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: StreamBuilder<int>(
                    stream: _pageIndicatorController!.stream, initialData: 0,
                    builder: (ctx, snapshot){
                      return AnimatedSmoothIndicator(
                        activeIndex: snapshot.data!,
                        effect:const ExpandingDotsEffect(
                          activeDotColor: Color(0xFFFFB74D),
                          dotColor: Color(0xFFC7C9D9),
                          dotWidth: 12,
                          dotHeight: 7
                        ),
                        count: OnBoardingRepository.all.length,
                      );
                    }
                  )
                ),
              ]
            )
          ),
          Positioned(
            bottom: 106.0, right: 16.0, left: 16.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: CustomButton(
                    buttonColor: MatrialColor.primaryColor,
                    hightButton: 50,
                    radiusButton: 10,
                    titlebuttonColor: TextColor.white70,
                    titleButton: titlebutton,
                    onPressed: () async{
                      if(OnBoardingRepository.all.isNotEmpty){
                        if(_pageController!.page == OnBoardingRepository.all.length - 1){
                          _navigateToLogin();
                          return;
                        }
                        await _pageController?.nextPage(duration: _duration, curve: Curves.ease);

                        currentIndexPage==2?titlebutton='GET START':titlebutton='NEXT';
                        
                      }
                    },
                    // child: const Icon(Icons.arrow_forward_ios_outlined)
                  )
                )
              ]
            )
          )
        ]
      )
    );
  }
}
