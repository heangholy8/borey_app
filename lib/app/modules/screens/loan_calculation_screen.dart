import 'package:borey_app/app/core/constants/constant.dart';
import 'package:borey_app/widgets/content_wiget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoanCalcuationScreen extends StatefulWidget {
  const LoanCalcuationScreen({Key? key}) : super(key: key);

  @override
  State<LoanCalcuationScreen> createState() => _LoanCalcuationScreenState();
}

class _LoanCalcuationScreenState extends State<LoanCalcuationScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: Geometry.tableContentPadding,
          child: Column(
            children: [
              CustomHeader(
                titleHeader: "Loan Calculator",
                hight: 87,
              ),
              Row(
                children: const [
                  Text(
                    "Calculator your EMI with few steps",
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600),
                  ),
                  SizedBox(
                    width: 6,
                  ),
                  Icon(
                    Icons.info_outline_rounded,
                    size: 16,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
