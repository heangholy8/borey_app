import 'package:borey_app/app/modules/screens/verify_otp.dart';
import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/content_wiget.dart';
import 'package:borey_app/widgets/custom_button.dart';
import 'package:borey_app/widgets/custom_textfield.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ResetPassword extends StatefulWidget {
  const ResetPassword({ Key? key }) : super(key: key);

  @override
  State<ResetPassword> createState() => _ResetPasswordState();
  
}

class _ResetPasswordState extends State<ResetPassword> {
  TextEditingController? _phoneController;
  FocusNode? _focusNode;

  @override
  void initState() {

    super.initState();
    _phoneController = TextEditingController();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _phoneController?.dispose();
    _focusNode?.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Container(
            margin:const EdgeInsets.only(top: 20),
            child: Column(
              children: [
                HeaderOTP(),
                const SizedBox(height: 55,),
                Container(
                  margin:const EdgeInsets.all(16),
                  child: Column(
                    children:[
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text('Reset Password',style: TextStyle(fontSize: 20,color: TextColor.black,fontWeight: FontWeight.bold)),
                      ),
                      const SizedBox(height: 15,),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text('Enter the email associated with your account and we’ll send an email instructons to reset your password',style: TextStyle(fontSize: 15,color: TextColor.black54)),
                      ),
                      const SizedBox(height: 45,),
                      const Align(
                        alignment: Alignment.centerLeft,
                        child: Text('Phone Number',style: TextStyle(fontSize: 15,color: TextColor.black)),
                      ),
                      const SizedBox(height: 10,),
                      PhoneField(
                        controller: _phoneController,
                        focusNode: _focusNode,
                        hintText: '012-345-678',
                      ),
                      const SizedBox(height: 58,),
                      CustomButton(
                        widthButton: 207,
                        hightButton: 50,
                        buttonColor: MatrialColor.primaryColor,
                        radiusButton: 10,
                        titleButton: 'Send'.tr(),
                        titlebuttonColor:TextColor.white70,
                        onPressed: (){
                          Navigator.of(context).push(
                            PageRouteBuilder(
                              pageBuilder: (ctx, anim1, anim2){
                                return const VerifyOTP();
                              },
                              transitionsBuilder: (ctx, anim1, anim2, child){
                                return FadeTransition(
                                  opacity: anim1,
                                  child: child
                                );
                              }
                            )
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}