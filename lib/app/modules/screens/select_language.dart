
import 'package:borey_app/app/modules/screens/onboarding.dart';
import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/custom_button.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';


class languageScreen extends StatefulWidget {
  const languageScreen({Key? key}) : super(key: key);
  
  @override
  _languageScreenState createState() => _languageScreenState();
}

class _languageScreenState extends State<languageScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   
  }

  @override
  void dispose() {
    // TODO: implement dispose
  
  }
   
  var khmer = const Locale("en");
  void updateLanguage(Locale locale, BuildContext context) {
    context.setLocale(locale);

  }

  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: MatrialColor.Background_defauld,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 50,
                      ),
                      SizedBox(
                        height: 120,
                        width: 170,
                        child: Hero(
                          tag: 'LOGO',
                          //child: Image.asset('assets/png/logo_borey.png',
                          child: Image.asset('assets/png/logo-1.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                     // const SizedBox(height: 20,),
                      const Text("Choose a language",style: TextStyle(color: MatrialColor.black,fontSize: 18),),

                      const SizedBox(height: 35,),
                      Container(
                        margin:const EdgeInsets.all(16),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                height: 50,
                                child: Center(
                                  child: InkWell(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Flexible(flex: 1,child: Image.asset("assets/png/lagUnited.png")),
                                        const SizedBox(width: 10,),
                                        const Flexible(flex: 2,child: Text("English",style: TextStyle(fontSize: 18),)),
                                        const SizedBox(width: 10,),
                                        translate=="en"?const Flexible(flex: 1,child: Icon(Icons.check_circle_outline_outlined,size: 23,color: MatrialColor.primaryColor,)):Container(),
                                      ],
                                    ),
                                    onTap: () {
                                      updateLanguage(const Locale("en"), context);
                                    },
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: const[
                                    BoxShadow(
                                      color: Color.fromARGB(255, 214, 210, 210),
                                      blurRadius: 7,
                                      offset: Offset(1, 1),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const SizedBox(width: 16,),
                            Expanded(
                              child: Container(
                                height: 50,
                                child: Center(
                                  child: InkWell(
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Flexible(flex: 1,child: Image.asset("assets/png/Cambodia (KH).png")),
                                        const SizedBox(width: 10,),
                                        const Flexible(flex: 2,child: Text("ភាសារខ្មែរ",style: TextStyle(fontSize: 18),)),
                                        const SizedBox(width: 10,),
                                        translate=="km"?const Flexible(flex: 1,child: Icon(Icons.check_circle_outline_outlined,size: 23,color: MatrialColor.primaryColor,)):Container(),
                                      ],
                                    ),
                                    onTap: () {
                                      updateLanguage(const Locale("km"), context);
                                    },
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: const[
                                    BoxShadow(
                                      color: Color.fromARGB(255, 214, 210, 210),
                                      blurRadius: 7,
                                      offset: Offset(1, 1),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ] 
                        ),
                      ),

                      const SizedBox(height: 50,),                                  
                      //----------Login---------------
                      CustomButton(
                        hightButton: 50,
                        buttonColor: MatrialColor.primaryColor,
                        maginRight: 16,
                        maginleft: 16,
                        radiusButton: 10,
                        titleButton: 'Continue',
                        titlebuttonColor: TextColor.white,
                        onPressed: (){
                          Navigator.of(context).push(
                            PageRouteBuilder(
                              pageBuilder: (ctx, anim1, anim2){
                                return const OnBoardingScreen();
                              },
                              transitionsBuilder: (ctx, anim1, anim2, child){
                                return FadeTransition(
                                  opacity: anim1,
                                  child: child
                                );
                              }
                            )
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
