import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/content_wiget.dart';
import 'package:borey_app/widgets/custom_button.dart';
import 'package:borey_app/widgets/custom_textfield.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

final _formKey = GlobalKey<FormState>();
TextEditingController? _firstName;
TextEditingController? _lastName;
TextEditingController? _phoneNumber;
TextEditingController? _email;
TextEditingController? _location;
TextEditingController? _cityCode;
TextEditingController? _workPlace;
TextEditingController? _rold;
TextEditingController? _salary;
int _valueSingle = 1;
class RegisterMember extends StatefulWidget {
  const RegisterMember({ Key? key }) : super(key: key);

  @override
  State<RegisterMember> createState() => _RegisterMemberState();
}

class _RegisterMemberState extends State<RegisterMember> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Container(
            color:const Color(0xFFFFFFFF),
           child: Column(
             children: <Widget>[
               Container(
                 child: CustomHeader(
                   onPressedArrowBack: (){
                    
                   },
                   colorArrowback: 0xFFFFA726,
                   titleHeader: 'Register Member',
                   hight: 86,
                 ),
               ),
               const SizedBox(height: 10,),
               Expanded(
                 child: SingleChildScrollView(
                   child: Form(
                      key: _formKey,
                     child: Container(
                       margin:const EdgeInsets.all(16),
                       child: Column(
                          children: <Widget>[
                            Row(
                               mainAxisAlignment:
                                                MainAxisAlignment
                                                    .spaceBetween,
                              children:<Widget> [
                                Expanded(
                                  child: CustomTextField(
                                    controller: _firstName,
                                    labelText: 'នាមខ្លួន'+'*',
                                  ),
                                ),
                                const SizedBox(width: 16,),
                                Expanded(
                                  child: CustomTextField(
                                    controller: _lastName,
                                    labelText: 'នាមត្រកូល'+'*',
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 16,),
                            CustomTextField(
                              controller: _phoneNumber,
                              labelText: 'លេខទូរស័ព្ទ',
                            ),
                            const SizedBox(height: 16,),
                            CustomTextField(
                              controller: _email,
                              labelText: 'អ៊ីម៉ែល',
                            ),
                            const SizedBox(height: 16,),
                            CustomTextField(
                              maxLines: 4,
                              controller: _location,
                              labelText: 'អាស័យដ្ឋាន',
                            ),
                            const SizedBox(height: 16,),
                            CustomTextField(
                              controller: _cityCode,
                              labelText: 'លេខកូដតំបន់',
                            ),
                            const SizedBox(height: 16,),
                            CustomTextField(
                              controller: _workPlace,
                              labelText: 'ទីកន្លែងការងារ',
                            ),
                            const SizedBox(height: 16,),
                            CustomTextField(
                              controller: _rold,
                              labelText: 'តួនាទី',
                            ),
                            const SizedBox(height: 16,),
                            CustomTextField(
                              controller: _salary,
                              labelText: 'ប្រាក់ខែ',
                            ),
                            const SizedBox(height: 16,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Radio(
                                      activeColor:const Color(0xFFFFA726),
                                      value: 1,
                                      groupValue: _valueSingle,
                                      onChanged: (newValue) =>
                                          setState(() => _valueSingle = newValue as int),
                                    ),
                                    Text(
                                      "នៅលីវ".tr(),
                                      style: const TextStyle(fontSize: 16,),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  width: 16,
                                ),
                                Row(
                                  children: [
                                    Radio(
                                      activeColor:const Color(0xFFFFA726),
                                      value: 2,
                                      groupValue: _valueSingle,
                                      onChanged: (newValue) =>
                                          setState(() => _valueSingle = newValue as int),
                                    ),
                                    Text(
                                      "មានគ្រួសារ".tr(),
                                      style: const TextStyle(fontSize: 16),
                                    )
                                  ],
                                ),
                                
                              ],
                            ),
                            const SizedBox(height: 30,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    CustomButton(
                                      widthButton: 123,
                                      hightButton: 55,
                                      buttonColor: MatrialColor.primaryColor,
                                      radiusButton: 15,
                                      titleButton: 'បោះបង់'.tr(),
                                      titlebuttonColor: TextColor.white70,
                                      onPressed: (){

                                      },
                                    ),
                                    const SizedBox(width: 16,),
                                    CustomButton(
                                      widthButton: 123,
                                      hightButton: 55,
                                      buttonColor: MatrialColor.primaryColor,
                                      radiusButton: 15,
                                      titleButton: 'ចុះឈ្មោះ'.tr(),
                                      titlebuttonColor: TextColor.white70,
                                      onPressed: (){
                                        
                                      },
                                    ),
                                  ],
                                )
                          ],
                       ),
                     ),
                   ),
                 ),
               ),
             ],
           ),
          ),
        ),
      ),
    );
  }
}