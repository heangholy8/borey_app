import 'dart:async';
import 'dart:ffi';
import 'dart:typed_data';
import 'package:borey_app/models/boreyMap.dart';
import 'package:borey_app/widgets/content_wiget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import "package:font_awesome_flutter/font_awesome_flutter.dart";
import 'package:http/http.dart' as http;
import 'dart:ui' as ui;

class maps extends StatefulWidget {
  @override
  mapsState createState() => mapsState();
}

class mapsState extends State<maps> {
  BitmapDescriptor? mapMarker;
  final Completer<GoogleMapController> _controller = Completer();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  Map<CircleId, Circle> circles = <CircleId, Circle>{};
  late String _mapStyle;
  @override
  void initState() {
    _add();
    _setCircles();
    rootBundle.loadString('assets/dark_map_style.json').then((string) {
      _mapStyle = string;
    });
    super.initState();
    setCustomMarker();
    BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), 'assets/png/current_location_icons.png');
  }

  void setCustomMarker() async {
    mapMarker = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(), 'assets/png/current_location_icons.png');
  }

  void _setCircles() {
    for (int i = 0; i < MAPS_DATA.length; i++) {
      final CircleId circleId = CircleId(MAPS_DATA[i].id);
      final Circle circle = Circle(
        circleId: circleId,
        center: LatLng(MAPS_DATA[i].lat, MAPS_DATA[i].long),
        onTap: () {},
        consumeTapEvents: false,
        radius: 900,
        strokeWidth: 0,
        fillColor: const Color.fromRGBO(201, 148, 44, 0.41),
      );
      if (mounted) {
        setState(() {
          circles[circleId] = circle;
        });
      }
    }
  }

  void _add() async {
    setState(() async {
      for (int i = 0; i < MAPS_DATA.length; i++) {
        final MarkerId markerId = MarkerId(MAPS_DATA[i].id);
        final Marker marker = Marker(
          markerId: markerId,
          position: LatLng(MAPS_DATA[i].lat, MAPS_DATA[i].long),
          icon: await BitmapDescriptor.fromAssetImage(
              const ImageConfiguration(),
              'assets/png/current_location_icons.png'),
          infoWindow:
              InfoWindow(title: MAPS_DATA[i].name, snippet: MAPS_DATA[i].deis),
          onTap: () {},
        );
        if (mounted) {
          setState(() {
            markers[markerId] = marker;
          });
        }
      }
    });
  }

  double zoomVal = 12;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              child: CustomHeader(
                onPressedArrowBack: () {
                  setState(() {});
                },
                colorArrowback: 0xFFFFA726,
                titleHeader: 'Locations',
                hight: 86,
              ),
            ),
            const SizedBox(
              height: 3.0,
            ),
            Expanded(
              child: Stack(
                children: <Widget>[
                  _buildGoogleMap(context),
                  Column(
                    children: [_zoomplusfunction(), _zoomminusfunction()],
                  ),
                  _buildContainer(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _zoomminusfunction() {
    return Align(
      alignment: Alignment.topRight,
      child: IconButton(
          icon: const Icon(FontAwesomeIcons.searchMinus, color: Colors.grey),
          onPressed: () {
            zoomVal--;
            _minus(zoomVal);
          }),
    );
  }

  Widget _zoomplusfunction() {
    return Align(
      alignment: Alignment.topRight,
      child: IconButton(
          icon: const Icon(FontAwesomeIcons.searchPlus, color: Colors.grey),
          onPressed: () {
            zoomVal++;
            _plus(zoomVal);
          }),
    );
  }

  Future<void> _minus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: const LatLng(11.5757927, 104.9211087), zoom: zoomVal)));
  }

  Future<void> _plus(double zoomVal) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: const LatLng(11.5757927, 104.9211087), zoom: zoomVal)));
  }

  Widget _buildContainer() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 20.0),
        height: 150.0,
        child: ListView.builder(
          itemCount: MAPS_DATA.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            final Boreys borey = MAPS_DATA[index];
            return _boxes(borey);
          },
        ),
      ),
    );
  }

  Widget _boxes(Boreys boreys) {
    return GestureDetector(
      onTap: () {
        _gotoLocation(boreys.lat, boreys.long);
      },
      child: Container(
        margin: const EdgeInsets.all(10),
        child: FittedBox(
          child: Material(
              color: const Color(0xFFFFFFFF),
              elevation: 14.0,
              borderRadius: BorderRadius.circular(24.0),
              shadowColor: const Color(0xFF383737),
              child: Row(
                children: [
                  Container(
                    width: 180,
                    height: 200,
                    child: ClipRRect(
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(24),
                          bottomLeft: Radius.circular(24)),
                      child: Image(
                        fit: BoxFit.fill,
                        image: NetworkImage(boreys.image),
                      ),
                    ),
                  ),
                  Container(
                    width: 340,
                    height: 200,
                    padding:
                        const EdgeInsets.only(left: 15, right: 15, bottom: 4),
                    child: myDetailsContainer1(boreys),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget myDetailsContainer1(Boreys meals) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            child: Text(
          meals.name,
          style: const TextStyle(
              color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.bold),
        )),
        Container(
            margin: const EdgeInsets.only(top: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                    child: Text(
                  meals.rating.toString(),
                  style: const TextStyle(
                      color: Colors.orange,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                )),
                const SizedBox(
                  width: 8,
                ),
                // RatingBar.builder(
                //   initialRating: meals.rating,
                //   minRating: 1,
                //   direction: Axis.horizontal,
                //   allowHalfRating: true,
                //   updateOnDrag: false,
                //   ignoreGestures: true,
                //   itemSize: 35,
                //   itemCount: 5,
                //   itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                //   itemBuilder: (context, _) =>
                //       Icon(
                //         Icons.star,
                //         color: Colors.amber,
                //         size: 10,
                //       ),
                //   onRatingUpdate: (double value) {},
                // ),
                const SizedBox(
                  width: 8,
                ),
                Container(
                    child: Text(
                  "(${meals.numberOrders})",
                  style: const TextStyle(
                      color: Colors.orange,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                )),
              ],
            )),
        Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              meals.deis,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: const TextStyle(
                color: Colors.black,
                fontSize: 18.0,
              ),
            )),
        Container(
            margin: const EdgeInsets.only(top: 8),
            child: const Text(
              "Closed 12:54  Opens 17:00 Thu",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
            )),
      ],
    );
  }

  Widget _buildGoogleMap(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        zoomControlsEnabled: false,
        mapType: MapType.normal,
        initialCameraPosition: const CameraPosition(
            target: LatLng(11.5757927, 104.9211087), zoom: 12),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
          controller.setMapStyle(_mapStyle);
        },
        markers: Set<Marker>.of(markers.values),
        circles: Set<Circle>.of(circles.values),
      ),
    );
  }

  Future<void> _gotoLocation(double lat, double long) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(lat, long),
      zoom: 13,
      tilt: 50.0,
      bearing: 45.0,
    )));
  }
}
