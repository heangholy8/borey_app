
import 'package:borey_app/app/modules/screens/loginPage.dart';
import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/custom_bottomsheet.dart';
import 'package:borey_app/widgets/custom_button.dart';
import 'package:borey_app/widgets/custom_textfield.dart';
import 'package:borey_app/widgets/translate.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';




TextEditingController? _firstName;
TextEditingController? _lastName;
TextEditingController? _phoneNumberEmail;
class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey();
   late final String titlebutton;
  late final String imagebutton;
  late final VoidCallback? onPressed;
  bool _isObscure = true;
  @override
  Widget build(BuildContext context) {

    //----------Text fileEmail...........
    final textfieldPassword = TextField(
      obscureText: _isObscure,
      decoration: InputDecoration( 
        fillColor: Colors.white,
        filled: true,
        contentPadding:const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        enabledBorder: OutlineInputBorder(borderRadius:BorderRadius.circular(15.0),
        borderSide:const BorderSide(color: Colors.white, width: 3.0)), 
        focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(15.0),
        borderSide:const BorderSide(color: Color(0xFFFFA726),
        )
      ),
      // labelText: "Email or Phone", //babel text
      hintText: "Password", //hint text
      hintStyle:const TextStyle(fontSize: 15,), //hint text style
      labelStyle:const TextStyle(fontSize: 13,), //label style
      suffixIcon: IconButton(
        icon: Icon(
          _isObscure ?Icons.visibility : Icons.visibility_off,),
          onPressed: (){
            setState(() {
              _isObscure = !_isObscure;
            });
          },
      ),
  )
    );

    //---------End-----------------

    //----------Text fileEmail...........
    final textfieldConfirmPassword = TextField(
      obscureText: _isObscure,
      decoration: InputDecoration( 
        fillColor: Colors.white,
        filled: true,
        contentPadding:const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        enabledBorder: OutlineInputBorder(borderRadius:BorderRadius.circular(15.0),
        borderSide:const BorderSide(color: Colors.white, width: 3.0)), 
        focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(15.0),
        borderSide:const BorderSide(color: Color(0xFFFFA726),
        )
      ),
      // labelText: "Email or Phone", //babel text
      hintText: "Confirm Password", //hint text
      hintStyle:const TextStyle(fontSize: 15,), //hint text style
      suffixIcon: IconButton(
        icon: Icon(
          _isObscure ?Icons.visibility : Icons.visibility_off,),
          onPressed: (){
            setState(() {
              _isObscure = !_isObscure;
            });
          },
      ),
  )
    );

    //---------End-----------------

    //----------SingUp-----------
    final signup = TextButton(
      onPressed: (){
        Navigator.of(context).push(
          PageRouteBuilder(
            pageBuilder: (ctx, anim1, anim2){
              return const LoginPage();
            },
            transitionsBuilder: (ctx, anim1, anim2, child){
              return FadeTransition(
                opacity: anim1,
                child: child
              );
            }
          )
        );
      },
      child: const Text('Login',style: TextStyle(fontSize: 13,color: Colors.blue,))
    );
    //----------End-----------

    return Material(
      child: Scaffold(
        body: SafeArea(
          child: Container(
            color: Colors.white,
            child: Center(
              child: Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 40,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                height: 50,
                                width: 65,
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      bottomLeft: Radius.circular(15.0),
                                    )),
                                child: TextButton(
                                  // onPressed: () =>  showAlertDialog(context),
                                  onPressed: () {
                                    const content = Padding(
                                      padding: EdgeInsets.symmetric(horizontal: 26.0, vertical: 56.0),
                                      child: Translate()
                                    );
                                    DraggableBottomSheetBuilder().show(context, content);                                    
                                  },
                                   child: SvgPicture.asset("assets/svg/Vector.svg",
                                      color: Colors.black,
                                    ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 110,
                            width: 110,
                            child: Hero(
                              tag: 'LOGO',
                              child: Image.asset(
                                  'assets/png/logo_borey.png',
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 30,
                          ),
                                     
                          Form(
                            key: _formKey,
                            child: Center(
                              child: Container(
                                margin:const EdgeInsets.only(left: 16,right: 16,bottom: 16),
                                child: Column(
                                  children:<Widget>[
                                     const SizedBox(
                                      height: 10,
                                    ),
                                    //-----------------name----------
                                    Row(
                                       mainAxisAlignment:MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(child: CustomTextField(
                                          controller: _firstName,
                                          labelText: 'នាមខ្លួន'+'*',
                                          ),
                                        ),
                                        const SizedBox(
                                          width: 16,
                                        ),
                                        Expanded(child: CustomTextField(
                                          controller: _lastName,
                                          labelText: 'នាមត្រកូល'+'*',
                                          ),
                                        ),
                                      ],
                                    ),
                                    //---------------End--------------
                                    const SizedBox(
                                      height: 16,
                                    ),
                                    //-------Email-------
                                    CustomTextField(
                                      controller: _phoneNumberEmail,
                                      labelText: 'អ៊ីម៉ែល/លេខទូរស័ព្ទ',
                                    ),
                                    //------End--------

                                    const SizedBox(
                                      height: 16,
                                    ),

                                    //--------Pass-------
                                    PasswordField(
                                      obscureText: _isObscure,
                                      hintText: 'Password',
                                      iconButton: IconButton(
                                        icon: Icon(
                                          _isObscure ?Icons.visibility : Icons.visibility_off,),
                                          onPressed: (){
                                            setState(() {
                                              _isObscure = !_isObscure;
                                            });
                                          },
                                      ),
                                    ),
                                    const SizedBox(height: 16,),
                                    PasswordField(
                                      obscureText: _isObscure,
                                      hintText: 'Confirm Password',
                                      iconButton: IconButton(
                                        icon: Icon(
                                          _isObscure ?Icons.visibility : Icons.visibility_off,),
                                          onPressed: (){
                                            setState(() {
                                              _isObscure = !_isObscure;
                                            });
                                          },
                                      ),
                                    ),
                                    //-------End---------
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          height: 1,
                                          width: 75,
                                          color: Colors.grey
                                        ),
                                        Container(
                                          margin:const EdgeInsets.only(left: 7,right: 7),
                                          child: Text("OR".tr()),
                                        ),
                                        Container(
                                          height: 1,
                                          width: 75,
                                          color: Colors.grey
                                        )
                                        
                                      ],
                                    ),
                                     const SizedBox(
                                        height: 15,
                                      ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children:<Widget>[
                                      //-------signupicloud-------
                                      CustomIcloudButton(
                                        imagebutton: "assets/svg/path4.svg",
                                        onPressed: (){
                                          
                                        },
                                      ),
                                      //-------End-------
                                      const SizedBox(
                                        width: 30,
                                      ),
                                      //-------signupGoogle-------
                                      CustomGoogleButton(
                                        imagebutton: 'assets/svg/logo googleg 48dp.svg',
                                        onPressed: (){
                                          
                                        },
                                      ),
                                      //-------End-------
                                      const SizedBox(
                                        width: 30,
                                      ),
                                      //-------signupFacebook-------
                                      CustomFaceButton(
                                        imagebutton: null,
                                        onPressed: (){

                                        },
                                      ),
                                      //-------End-------
                                      ],
                                    ),  
                                  ],
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          //----------Login---------------
                          CustomButton(
                            hightButton: 50,
                            buttonColor: MatrialColor.primaryColor,
                            maginRight: 16,
                            maginleft: 16,
                            radiusButton: 25,
                            titleButton: 'SIGN UP',
                            titlebuttonColor: TextColor.white70,
                            onPressed: (){
                            
                            },
                          ),
                          //---------End----------
                          const SizedBox(
                            height: 15,
                          ),

                          //--------SignUp-------
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Already have account?".tr(),style:const TextStyle(fontSize: 13,fontWeight: FontWeight.bold),),
                              signup,
                            ],
                          ),
                          const SizedBox(
                            height: 25,
                          ),
                
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
