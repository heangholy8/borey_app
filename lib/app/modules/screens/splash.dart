import 'dart:async';

import 'package:borey_app/app/modules/screens/select_language.dart';
import 'package:borey_app/style/material_color.dart';
import 'package:flutter/material.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(const Duration(seconds: 3),
            () => Navigator.of(context).pushReplacement(
      PageRouteBuilder(
        pageBuilder: (ctx, anim1, anim2){
          return const languageScreen();
        },
        transitionsBuilder: (ctx, anim1, anim2, child){
          return FadeTransition(
            opacity: anim1,
            child: child
          );
        })
      )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        decoration: const BoxDecoration(
          color: MatrialColor.primaryColor,
        ),
        child: Stack(
          children: [
            Positioned.fill(
              bottom: 126.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Flexible(
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 75.0,
                      child: Hero(
                        tag: 'LOGO',
                        child: Image.asset('assets/png/logo_borey.png', width: 100.0, height: 100.0,)
                      )
                    )
                  ),
                  const SizedBox(height: 36.0),
                  const Flexible(
                    child: Text('Welcome', style: TextStyle(
                      fontSize: 20.0, fontWeight: FontWeight.bold
                    ))
                  )
                ]
              )
            ),
            const SizedBox(height: 550,),
            const Positioned(
              bottom: 36.0, left: 0, right: 0,
              child: Center(
                child: Text('Powered by CAMIS', style: TextStyle(
                  color: Colors.black54, fontWeight: FontWeight.bold
                ))
              )
            )
          ]
        )
      )
    );
  }
}
