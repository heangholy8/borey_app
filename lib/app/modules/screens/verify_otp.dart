import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/content_wiget.dart';

import 'package:borey_app/widgets/custom_button.dart';
import 'package:borey_app/widgets/otp.dart';
import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/material.dart';


class VerifyOTP extends StatefulWidget {
  const VerifyOTP({ Key? key }) : super(key: key);

  @override
  State<VerifyOTP> createState() => _VerifyOTPState();
  
}

class _VerifyOTPState extends State<VerifyOTP> {
 final String phoneNumber='12345678';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: MatrialColor.Background_defauld,
          child: Container(
            margin:const EdgeInsets.only(top: 20),
            child: Column(
              children: [
                HeaderOTP(),
                const SizedBox(height: 15,),
                
                const SizedBox(height: 55,),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      margin:const EdgeInsets.all(16),
                      child: Column(
                        children:[
                          const Align(
                            alignment: Alignment.centerLeft,
                            child: Text('OTP Verification',style: TextStyle(fontSize: 20,color: TextColor.black)),
                          ),
                          const SizedBox(height: 15,),
                          const Align(
                            alignment: Alignment.centerLeft,
                            child: Text('Please enter the number code send to  your message box',style: TextStyle(fontSize: 15,color: TextColor.black54)),
                          ),
                          const SizedBox(height: 25,),
                          Row(
                            children: [
                              RichText(
                                text: TextSpan(
                                    text: "Enter the code sent to: ",
                                    children: [
                                      TextSpan(
                                          text: phoneNumber,
                                          style:const TextStyle(
                                              color: TextColor.primaryColor,
                                              fontSize: 15)),
                                    ],
                                    style:const TextStyle(
                                      color: TextColor.black87,
                                              fontSize: 15
                                    )),
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                          const SizedBox(height: 35,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              fieldOTP(first: true, last: false),
                              fieldOTP(first: false, last: false),
                              fieldOTP(first: false, last: false),
                              fieldOTP(first: false, last: false),
                              fieldOTP(first: false, last: false),
                              fieldOTP(first: false, last: false),
                            ],
                          ),
                          const SizedBox(height: 58,),
                          CustomButton(
                            hightButton: 50,
                            buttonColor: MatrialColor.primaryColor,
                            radiusButton: 10,
                            titleButton: 'Verify'.tr(),
                            titlebuttonColor: TextColor.white70,
                            onPressed: (){
                              
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}