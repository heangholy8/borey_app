// ignore_for_file: file_names

import 'package:borey_app/app/modules/screens/verify_otp.dart';
import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/custom_button.dart';
import 'package:borey_app/widgets/custom_textfield.dart';
import 'package:flutter/material.dart';




class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);
  
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final GlobalKey<FormState> _formKey = GlobalKey();
  TextEditingController? _phoneController;
  FocusNode? _focusNode;

  @override
  void initState() {
    super.initState();
    _phoneController = TextEditingController();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {

    _phoneController?.dispose();
    _focusNode?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: MatrialColor.Background_defauld,
          child: Column(
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 50,
                      ),
                      SizedBox(
                        height: 110,
                        width: 110,
                        child: Hero(
                          tag: 'LOGO',
                          child: Image.asset('assets/png/logo_borey.png',
                            fit: BoxFit.contain,
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 50,
                      ),                                  
                      Form(
                        key: _formKey,
                        child: Center(
                          child: Container(
                            margin:const EdgeInsets.only(left: 16,right: 16,bottom: 16),
                            child: Column(
                              children:<Widget>[
                                const SizedBox(height: 65,),
                                const Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text('Please enter your Phone number',style: TextStyle(fontSize: 18,color: TextColor.black)),
                                ),
                                const SizedBox(height: 45,),
                                const Align(
                                  alignment: Alignment.centerLeft,
                                  child: Text('Phone Number',style: TextStyle(fontSize: 15,color: TextColor.black)),
                                ),
                                const SizedBox(height: 10,),
                                PhoneField(
                                  controller: _phoneController,
                                  focusNode: _focusNode,
                                  hintText: '012-345-678',
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      //----------Login---------------
                      CustomButton(
                        hightButton: 50,
                        buttonColor: MatrialColor.primaryColor,
                        maginRight: 16,
                        maginleft: 16,
                        radiusButton: 10,
                        titleButton: 'Continue',
                        titlebuttonColor: TextColor.white,
                        onPressed: (){
                          Navigator.of(context).push(
                            PageRouteBuilder(
                              pageBuilder: (ctx, anim1, anim2){
                                return const VerifyOTP();
                              },
                              transitionsBuilder: (ctx, anim1, anim2, child){
                                return FadeTransition(
                                  opacity: anim1,
                                  child: child
                                );
                              }
                            )
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
