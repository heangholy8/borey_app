import 'package:borey_app/models/view/map_view_detail.dart';
import 'package:borey_app/widgets/custom_container.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({ Key? key }) : super(key: key);

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin:const EdgeInsets.all(15),
              height: 230,
              child:const MapViewDetail(
                latitude: 11.5548969,
                longitude: 104.9097405,
              ),
            ),
            Container_FirstShadow(
              height: 100,
              width: 250,
              child: Container(),
            ),
            SizedBox(height: 10,),
            Container_SecondShadow(
              height: 200,
              width: 150,
              child: Container(),
            ),
            SizedBox(height: 10,),
            Container_UnShadow(
              height: 100,
              width: 200,
              child: Container(),
            )
             
          ],
        ),
      ),
    );
  }
}