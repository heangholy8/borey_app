
import 'package:borey_app/app/modules/screens/home_page.dart';
import 'package:borey_app/app/modules/screens/loginPage.dart';
import 'package:borey_app/app/modules/screens/signup_page.dart';
import 'package:flutter/material.dart';

class AppRouter {   
  
  Route generateRoute(RouteSettings setting) {
    switch (setting.name) {
      case '/':
        return MaterialPageRoute(builder: (ctx) => const HomeScreen());
      case '/login':
        return MaterialPageRoute(builder: (ctx) => const LoginPage());
      case '/signup':
        return MaterialPageRoute(builder: (ctx) => const SignUpPage());
      default:
        return MaterialPageRoute(builder: (ctx) => const LoginPage());
    }
  }
}