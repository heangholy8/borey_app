import 'package:flutter/material.dart';

mixin Toast {
  /// for loading when call api
  Future<T>? showLoading<T>(Future<T> Function() callback) {
    // return Get.showOverlay<T>(
    //   asyncFunction: callback,
    //   loadingWidget: Dialog(
    //     child: Container(
    //       width: 200,
    //       height: 200,
    //       alignment: Alignment.center,
    //       child: SpinKitDoubleBounce(
    //         color: Get.theme.primaryColor,
    //       ),
    //     ),
    //   ),
    // );
  
  
  }


  /// for Error Dialog
  void showErrorDialog({String? title, String? content}) {
    // Get.dialog(
    //   Dialog(
    //     child: SizedBox(
    //       width: 200,
    //       child: Padding(
    //         padding: const EdgeInsets.symmetric(horizontal: 15),
    //         child: Column(
    //           mainAxisSize: MainAxisSize.min,
    //           children: [
    //             const SizedBox(height: 15),
    //             Row(
    //               mainAxisSize: MainAxisSize.min,
    //               children: [
    //                 const Icon(
    //                   Icons.cancel_outlined,
    //                   color: Colors.redAccent,
    //                 ),
    //                 const SizedBox(width: 10),
    //                 Text(
    //                   title ?? LocaleKeys.error.tr,
    //                   style: const TextStyle(
    //                     fontSize: 15,
    //                     color: Colors.redAccent,
    //                   ),
    //                 ),
    //               ],
    //             ),
    //             const SizedBox(height: 15),
    //             Text(
    //               content ?? LocaleKeys.sth_wrong.tr,
    //               textAlign: TextAlign.center,
    //               style: const TextStyle(
    //                 fontSize: 14,
    //               ),
    //             ),
    //             const SizedBox(height: 10),
    //             TextButton(
    //               onPressed: () => Get.back(),
    //               child: Text(
    //                 LocaleKeys.cancel.tr,
    //               ),
    //             ),
    //             const SizedBox(height: 15),
    //           ],
    //         ),
    //       ),
    //     ),
    //   ),
    // );
 
  }

  /// Show Error Snackbar
  void showErrorSnackBar(String title, String message, {Widget? icon}) {
    // Get.snackbar(
    //   title,
    //   message,
    //   icon: icon ?? const Icon(Icons.notifications),
    //   backgroundColor: Colors.redAccent,
    //   maxWidth: 400,
    //   colorText: Colors.white,
    //   borderRadius: 5,
    //   margin: EdgeInsets.only(top: 10, left: Get.size.width - 410),
    //   mainButton: TextButton(
    //     onPressed: () => Get.back(),
    //     child: const Icon(
    //       Icons.close,
    //       color: Colors.white,
    //     ),
    //   ),
    // );
  
  }

  /// Show Success SnackBar
  void showSuccessSnackBar(String title, String message, {Widget? icon}) {
    // Get.snackbar(
    //   title,
    //   message,
    //   icon: icon ?? const Icon(Icons.notifications),
    //   backgroundColor: Colors.green,
    //   maxWidth: 400,
    //   colorText: Colors.white,
    //   borderRadius: 5,
    //   margin: EdgeInsets.only(top: 10, left: Get.size.width - 410),
    //   mainButton: TextButton(
    //     onPressed: () => Get.back(),
    //     child: const Icon(
    //       Icons.close,
    //       color: Colors.white,
    //     ),
    //   ),
    // );
 
  }


  // Show MenuDialog
  void showMenuDialog(List<Widget> menus, {String? title}) {
    // Get.dialog(
    //   Dialog(
    //     child: SizedBox(
    //       width: 200,
    //       child: Column(
    //         mainAxisSize: MainAxisSize.min,
    //         children: [
    //           if (title != null)
    //             Padding(
    //               padding: const EdgeInsets.only(top: 10, bottom: 5),
    //               child: Text(
    //                 title,
    //                 style: Get.theme.textTheme.headline6!.copyWith(fontSize: 15, color: Get.theme.primaryColor),
    //               ),
    //             ),
    //           ...menus,
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  
  }

  // Show Dialog Before Update or Delete 
  void showDeleteUpdateDialog({
    Function()? onUpdate,
    Function()? onDelete,
    String? title,
    String? deleteTitle,
    String? deleteContent,
  }) {
    // Get.dialog(
    //   Dialog(
    //     child: SizedBox(
    //       width: 200,
    //       child: Column(
    //         mainAxisSize: MainAxisSize.min,
    //         children: [
    //           if (title != null)
    //             Padding(
    //               padding: const EdgeInsets.only(top: 10, bottom: 5),
    //               child: Text(
    //                 title,
    //                 style: Get.theme.textTheme.headline6!.copyWith(fontSize: 15, color: Get.theme.primaryColor),
    //               ),
    //             ),
    //           ListTile(
    //             title: Text(LocaleKeys.update.tr),
    //             leading: Icon(
    //               Icons.edit,
    //               color: Get.theme.primaryColor,
    //             ),
    //             onTap: () {
    //               Get.back();
    //               if (onUpdate != null) {
    //                 onUpdate();
    //               }
    //             },
    //           ),
    //           ListTile(
    //             title: Text(LocaleKeys.delete.tr),
    //             leading: const Icon(
    //               Icons.delete,
    //               color: Colors.redAccent,
    //             ),
    //             onTap: () {
    //               showConfirmDialog(
    //                 title: deleteTitle,
    //                 content: deleteContent,
    //                 onOk: () {
    //                   Get.back();
    //                   Get.back();
    //                   if (onDelete != null) {
    //                     onDelete();
    //                   }
    //                 },
    //               );
    //             },
    //           ),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  
  }

  /// Show ConfirmDialog 
  void showConfirmDialog({String? title, String? content, required void Function()? onOk}) {
    // Get.dialog(
    //   Dialog(
    //     child: SizedBox(
    //       width: 200,
    //       child: Column(
    //         mainAxisSize: MainAxisSize.min,
    //         children: [
    //           const SizedBox(height: 15),
    //           Text(
    //             title ?? LocaleKeys.error.tr,
    //             style: const TextStyle(
    //               fontSize: 15,
    //               color: Colors.redAccent,
    //             ),
    //           ),
    //           const SizedBox(height: 15),
    //           Text(
    //             content ?? LocaleKeys.sth_wrong.tr,
    //             style: const TextStyle(
    //               fontSize: 14,
    //             ),
    //           ),
    //           const SizedBox(height: 10),
    //           Row(
    //             mainAxisAlignment: MainAxisAlignment.spaceAround,
    //             children: [
    //               TextButton(
    //                 onPressed: () => Get.back(),
    //                 child: Text(
    //                   LocaleKeys.cancel.tr,
    //                 ),
    //               ),
    //               TextButton(
    //                 onPressed: onOk,
    //                 child: Text(
    //                   LocaleKeys.okay.tr,
    //                 ),
    //               ),
    //             ],
    //           ),
    //           const SizedBox(height: 15),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
 
  }
}
