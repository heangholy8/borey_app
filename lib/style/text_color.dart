import 'dart:ui';

import 'package:flutter/material.dart';

abstract class TextColor{
  static const black = Colors.black;
  static const black87 = Color(0xdd000000);
  static const black54 = Color(0x8A000000);
  static const black45 = Color(0x73000000);
  static const white = Colors.white;
  static const white60 = Color(0x99ffffff);
  static const white70 = Color(0xb3ffffff);
  static const primaryColor = Color(0xFFF89520);
  static const Secondary = Color(0xFFC4996C);
}