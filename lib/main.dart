import 'dart:developer';
import 'package:borey_app/app/modules/screens/loan_calculation_screen.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'app/modules/screens/loginPage.dart';
import 'app/modules/screens/splash.dart';
import 'app/routes/main_routes.dart';
import 'style/material_color.dart';

bool isOnBoardingAlreadyViewed = false;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  EasyLocalization.logger.enableBuildModes = [];

  final prefs = await SharedPreferences.getInstance();
  if (prefs.containsKey('onBoarding-done')) {
    final bool? result = prefs.getBool('onBoarding-done');
    if (result!) {
      isOnBoardingAlreadyViewed = result;
    }
  }
  runApp(
    EasyLocalization(
      supportedLocales: const [Locale('km'), Locale('en')],
      path: 'assets/translations',
      startLocale: const Locale('en'),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final fontName = context.locale.toString();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: buildMaterialColor(const Color(0xFFFFA726)),
        fontFamily: fontName == "en" ? "manrope" : "battambang",
      ),
      onGenerateRoute: AppRouter().generateRoute,
      // home:
      //     !isOnBoardingAlreadyViewed ? const LoginPage() : const SplashScreen(),
      home: LoanCalcuationScreen(),
    );
  }
}
