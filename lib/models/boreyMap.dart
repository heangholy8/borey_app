class Boreys {
  String name;
  String id;
  String deis;
  String image;
  String numberOrders;
  double lat;
  double long;
  double rating;

  Boreys(this.name, this.id, this.deis, this.image,this.numberOrders, this.lat, this.long,
      this.rating);


}

List<Boreys> MAPS_DATA = [
  Boreys("Sen Sok", "1",
      "HI.",
      "https://images.realestate.com.kh/__sized__/listings/2021-05/2019-09-03__11-35-50__1_10-thumbnail-750x562-70.jpeg","152",
      11.5130623, 104.7810738,4
  ),
  Boreys("Tul kok", "2",
      "HI.",
      "https://images.realestate.com.kh/__sized__/listings/2021-05/2019-09-03__11-35-50__1_10-thumbnail-750x562-70.jpeg","123",
      11.5474552, 104.8423307,2.6
  ),
  Boreys("Phnom Penh thmey", "44",
      "HI",
      "https://images.realestate.com.kh/__sized__/listings/2021-05/2019-09-03__11-35-50__1_10-thumbnail-750x562-70.jpeg","223",
      11.5659537, 104.8626332,4.4
  ),
  Boreys("Boxcar Social, ", "34",
      " HI",
      "https://images.realestate.com.kh/__sized__/listings/2021-05/2019-09-03__11-35-50__1_10-thumbnail-750x562-70.jpeg","223",
      11.5659537, 104.8626332,4.4
  ),
  Boreys("Mean Chey", "3",
      "HI.",
      "https://images.realestate.com.kh/__sized__/listings/2021-05/2019-09-03__11-35-50__1_10-thumbnail-750x562-70.jpeg","143",
      11.5437214, 104.7644927,4
  ),
  Boreys("Doun Penh", "4",
      "HI ",
      "https://images.realestate.com.kh/__sized__/listings/2021-05/2019-09-03__11-35-50__1_10-thumbnail-750x562-70.jpeg","342",
      11.5445444, 104.6514869,4
  ),
];