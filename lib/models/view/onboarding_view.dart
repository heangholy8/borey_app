import 'package:borey_app/style/text_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../onboarding_model.dart';

class OnBoardingView extends StatelessWidget {
  final OnBoardingModel data;
  const OnBoardingView({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:const EdgeInsets.only(left: 16,right: 16),
      child: SizedBox(     
        width: double.infinity, height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Flexible(
              child: SvgPicture.asset(data.image)
            ),
            Flexible(
              child: Text(data.title, style: const TextStyle(
                fontWeight: FontWeight.bold, fontSize: 18.0,
                color: TextColor.black
              ))
            ),
            const SizedBox(height: 16.0),
            Flexible(
              child: Text(data.subTitle, style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: TextColor.black87
              ))
            ),
            const SizedBox(height: 76,)
          ]
        )
      ),
    );
  }
}
