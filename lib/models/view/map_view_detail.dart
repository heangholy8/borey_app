import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapViewDetail extends StatefulWidget {
 const MapViewDetail({Key? key,this.latitude,this.longitude}) : super(key: key);
  final double? latitude;
  final double? longitude;
  @override
  _MapViewDetailState createState() => _MapViewDetailState();
}

class _MapViewDetailState extends State<MapViewDetail> {
  Set<Marker> _markers = HashSet<Marker>();
  Set<Circle> _circles = HashSet<Circle>();
  bool _showMapStyle = false;

  late GoogleMapController _mapController;
  late BitmapDescriptor _markerIcon;

  @override
  void initState() {
    super.initState();
    _setCircles();
  }

  void _setCircles() {
    _circles.add(
      Circle(
          circleId: CircleId("0"),
          center: LatLng(widget.latitude!,widget.longitude!),
          radius: 330,
          strokeWidth: 0,
          fillColor:const Color.fromRGBO(201, 148, 44, 0.41)),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;

    setState(() {
      _markers.add(
        Marker(
            markerId: MarkerId("0"),
            position: LatLng(widget.latitude!,widget.longitude!),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ClipRRect(
        borderRadius:const BorderRadius.only(
        topLeft: Radius.circular(15),
        topRight: Radius.circular(15),
        bottomRight: Radius.circular(15),
        bottomLeft: Radius.circular(15),
        ),
        child: GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(
              target: LatLng(widget.latitude!,widget.longitude!),
              zoom: 15,
            ),
            // markers: _markers,
            circles: _circles,
            zoomControlsEnabled: false,
            mapToolbarEnabled: false,
          ),
      ),      
    );
  }
}