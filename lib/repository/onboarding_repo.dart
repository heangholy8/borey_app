import 'package:borey_app/models/onboarding_model.dart';

class OnBoardingRepository{
  static List<OnBoardingModel> get all{
    return const [
      OnBoardingModel(image: 'assets/onboarding/House searching-amico 1.svg', title: 'Find a House to Fit All Your Needs and Wants',
        subTitle: 'Easy to find a house that fit to your needs with Borey Phnom Penh Thmey mobile application.'),
      OnBoardingModel(image: 'assets/onboarding/Construction-pana 1.svg', title: 'Modern construction ',
        subTitle: 'We are a real estate developer that provides a modern design housing complex.'),
      OnBoardingModel(image: 'assets/onboarding/For sale-pana 1.svg', title: 'Meet our Agency',
          subTitle: 'Our agency will help you to find the best house for you. Please do not worries you can consult with them for FREE!'),
      
    ];
  }
}
