import 'package:flutter/material.dart';

import 'empty_wiget.dart';

class DraggableBottomSheetModel{
  double minChildSize;
  double maxChildSize;
  double initialChildSize;

  DraggableBottomSheetModel({this.maxChildSize = .85, this.minChildSize = .5,
    this.initialChildSize = .5});
}

class DraggableBottomSheetBuilder{
  DraggableBottomSheetModel? sheet;

  DraggableBottomSheetBuilder(){
    sheet = DraggableBottomSheetModel();
  }

  DraggableBottomSheetBuilder setInitialChildSize(double initialChildSize){
    sheet!.initialChildSize = initialChildSize;
    return this;
  }

  DraggableBottomSheetBuilder setMinChildSize(double midChildSize){
    sheet!.minChildSize = midChildSize;
    return this;
  }

  // DraggableBottomSheetBuilder build(){
  //   return this;
  // }

  void show(final BuildContext context, final Widget content, {final Widget? bottom}){
    showModalBottomSheet(
      useRootNavigator: true,
      context: context, isScrollControlled: true, elevation: 0.0,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)
        )
      ),
      builder: (ctx){
        final isKeyboardVisible = MediaQuery.of(ctx).viewInsets.bottom != 0;
        return SafeArea(
          bottom: false,
          child: DraggableScrollableSheet(
            snap: true,
            expand: false, minChildSize: isKeyboardVisible ? sheet!.maxChildSize : sheet!.minChildSize, maxChildSize: sheet!.maxChildSize,
            initialChildSize: isKeyboardVisible ? sheet!.maxChildSize : sheet!.initialChildSize,
            builder: (ctx, controller){
              return Column(
                children: [
                  const SizedBox(height: 16.0),
                  Center(
                    child: Container(
                      width: 50, height: 5,
                      decoration: BoxDecoration(
                        color: const Color(0xFF8F90A6),
                        borderRadius: BorderRadius.circular(50.0)
                      )
                    )
                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      controller: controller,
                      child: content
                    )
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: bottom ?? EmptyWidget.instance
                  )
                ]
              );
            }
          )
        );
      }
    );
  }
}