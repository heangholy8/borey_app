import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {
   final double? height;
  final double? width;
  final Widget? child;
  final EdgeInsets?margin;
  final EdgeInsets?padding;
  const CustomContainer({ Key? key,this.height,this.width,this.child,this.margin,this.padding }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      padding: padding,
      height: height,
      width: width,
      child: child,
    );
  }
}

class Container_FirstShadow extends CustomContainer {
  const Container_FirstShadow({ Key? key,final double? height,final double? width,final Widget? child,final EdgeInsets?margin,final EdgeInsets?padding,}) : 
  super(key: key, margin: margin,padding: padding,height: height,width: width,child: child,);

  @override
  Widget build(BuildContext context) {
    return Container(
      width:width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow:const [
                  BoxShadow(
                    color: Color(0xFFEEEEEE),
                    offset: Offset(8.0, 8.0), //(x,y)
                    blurRadius: 8.0,
                  ),
                ],
      ),
      child: child,
    );
  }
}
class Container_SecondShadow extends CustomContainer {
  const Container_SecondShadow({ Key? key,final double? height,final double? width,final Widget? child,final EdgeInsets?margin,final EdgeInsets?padding, }) : 
  super(key: key, margin: margin,padding: padding,height: height,width: width,child: child,);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow:const [
            BoxShadow(
              color: Color(0xFFEEEEEE),
              offset: Offset(2.0, 2.0), //(x,y)
              blurRadius: 1.0,
            ),
          ],
        ),
        child: child,
    );
  }
}
class Container_UnShadow extends CustomContainer {
  const Container_UnShadow({ Key? key,final double? height,final double? width,final Widget? child,final EdgeInsets?margin,final EdgeInsets?padding, }) : 
  super(key: key, margin: margin,padding: padding,height: height,width: width,child: child,);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: child,
    );
  }
}