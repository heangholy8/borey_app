import 'package:borey_app/style/text_color.dart';
import 'package:borey_app/widgets/custom_iconbutton.dart';
import 'package:flutter/material.dart';

class CustomHeader extends StatelessWidget {
  double? hight;
  String titleHeader;
  IconButton? iconButtonLeft;
  VoidCallback? onPressedArrowBack;
  VoidCallback? onPressed;
  int colorArrowback;
  CustomHeader(
      {Key? key,
      this.hight,
      this.iconButtonLeft,
      this.onPressedArrowBack,
      this.titleHeader = '',
      this.colorArrowback = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: hight,
      child: Row(
        children: <Widget>[
          const Flexible(
            flex: 1,
            child: Iconbuttonback(),
          ),
          Flexible(
            flex: 3,
            child: Container(
              child: Center(
                child: Text(
                  titleHeader,
                  style: const TextStyle(fontSize: 18, color: TextColor.black),
                ),
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
                // child: Center(
                //   child: IconButton( icon:const Icon(Icons.arrow_back),
                //   onPressed: onPressed,
                //   ),
                // ),
                ),
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: const Color(0xFFFFFFFF),
        // boxShadow: [
        //   BoxShadow(
        //     color: Colors.grey.withOpacity(0.4),
        //     spreadRadius: -1,
        //     blurRadius: 5,
        //     offset: const Offset(0, 1), // changes position of shadow
        //   ),
        // ],
      ),
    );
  }
}

class HeaderOTP extends StatelessWidget {
  HeaderOTP({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const Flexible(
          flex: 1,
          child: Center(child: Iconbuttonback()),
        ),
        Flexible(
          flex: 3,
          child: Container(
            child: Center(
              child: SizedBox(
                height: 50,
                width: 50,
                child: Hero(
                  tag: 'LOGO',
                  child: Image.asset(
                    'assets/png/logo_borey.png',
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(),
        ),
      ],
    );
  }
}
