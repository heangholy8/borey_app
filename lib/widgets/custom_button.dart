import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class CustomGoogleButton extends StatelessWidget {

  String imagebutton;
  VoidCallback? onPressed;
  CustomGoogleButton({ Key? key,required this.onPressed, required this.imagebutton }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color:Colors.grey[300],
        shape: BoxShape.circle,
      ),
      width: 53,
      height: 53,
     // margin: const EdgeInsets.only(right: 16,left: 16),
      child: MaterialButton(
        child: Center(
          child: SizedBox(
                child: SvgPicture.asset(
                          imagebutton,
                          fit: BoxFit.contain,),
              ),
        ),
        onPressed: onPressed
      ),
    );
  }
}

class CustomFaceButton extends CustomGoogleButton {

  CustomFaceButton({ Key? key,onPressed,imagebutton }) : super(key: key,onPressed: onPressed,imagebutton: imagebutton='');  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:const BoxDecoration(
        color:Colors.blue,
        shape: BoxShape.circle,
      ),
      width: 53,
      height: 53,
      //margin: const EdgeInsets.only(right: 16,left: 16),
      child: MaterialButton(
        child:const Center(
          child: Icon(
                Icons.facebook_outlined,
                color: Colors.white,
                size: 22.86,
              ),
        ),
        onPressed: onPressed
      ),
    );
  }
}

class CustomIcloudButton extends StatelessWidget {

  String imagebutton;
  VoidCallback? onPressed;
  CustomIcloudButton({ Key? key,required this.onPressed, required this.imagebutton }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:const BoxDecoration(
        color:Colors.black,
        shape: BoxShape.circle,
      ),
      width: 53,
      height: 53,
     // margin: const EdgeInsets.only(right: 16,left: 16),
      child: MaterialButton(
        child: Center(
          child: SizedBox(
                child: SvgPicture.asset(
                          imagebutton,
                          fit: BoxFit.contain,),
              ),
        ),
        onPressed: onPressed
      ),
    );
  }
}

class CustomButton extends StatelessWidget {

  String titleButton;
  double? hightButton;
  double? widthButton;
  double radiusButton;
  double maginRight;
  double maginleft;
  Color? titlebuttonColor;
  Color? buttonColor;
  VoidCallback? onPressed;
  CustomButton({ Key? key,this.titleButton="",this.onPressed ,this.hightButton,this.widthButton,this.radiusButton=0,this.maginRight=0,this.maginleft=0,this.buttonColor,this.titlebuttonColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widthButton,
      margin: EdgeInsets.only(right: maginRight,left: maginleft),
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(radiusButton))),
        height: hightButton,
       
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(child: Text(titleButton,style:TextStyle(color: titlebuttonColor,fontSize: 16),)),
          ],
        ),
         
        color: buttonColor,
        onPressed: onPressed,
      ),
    );
  }
}