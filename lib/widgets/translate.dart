import 'package:borey_app/style/text_color.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
class Translate extends StatefulWidget {
  const Translate({ Key? key }) : super(key: key);

  @override
  State<Translate> createState() => _TranslateState();
}

class _TranslateState extends State<Translate> {
  List<Map<String, dynamic>> langList = const [
    {"name": "English", "locate": Locale("en")},
    {"name": "ភាសាខ្មែរ", "locate": Locale("km")},
  ];
  var khmer = const Locale("en");
  void updateLanguage(Locale locale, BuildContext context) {
    context.setLocale(locale);
    Navigator.of(context).pop();
  }
  @override
  Widget build(BuildContext context) {
    final translate = context.locale.toString();
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Text("CHANGE_LANGUAGE".tr(),style:const TextStyle(fontSize: 20,color: TextColor.black,)),
        ),
        const SizedBox(height: 35,),
        ListView(
          physics:const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: (langList).map(
            (e) {
              return InkWell(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(e["name"].toString(),style:const TextStyle(fontSize: 18),),
                ),
                onTap: () {
                  updateLanguage(e["locate"], context);
                },
              );
            },
          ).toList(),
        ),
      ],
    );
  }
}
