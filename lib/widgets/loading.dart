import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  const Loading({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color:const Color(0xFFF0F0F0),
      child: Center(
        child: Container(
          height: 145,
          width: 145,
          decoration: BoxDecoration(
          color:const Color(0xB3FFFFFF),
          borderRadius: BorderRadius.circular(25.0),
          boxShadow:const [
            BoxShadow(
              color: Colors.white,
              offset: Offset(2.0, 2.0), //(x,y)
              blurRadius: 1.0,
            ),
          ],
          ),
          child: Column(children:const [
              
          ]),
        ),
      ),
    );
  }
}