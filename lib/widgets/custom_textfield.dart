import 'package:borey_app/style/text_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';

typedef OnFieldSubmitted = void Function(String? value);
typedef OnChanged = void Function(String? value);

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final TextInputType? textInputType;
  final String? hintText;
  final String? labelText;
  final OnFieldSubmitted? onFieldSubmitted;
  final OnChanged? onChanged;
  final FocusNode? focusNode;
  final int? maxLines;
  final bool expands;
  final bool errorState;
  const CustomTextField({Key? key, this.controller, this.onFieldSubmitted, this.onChanged, this.focusNode, this.maxLines = 1,
    this.expands = false, this.errorState = false,
    this.hintText = '',this.labelText = '', this.textInputType = TextInputType.text, IconButton? iconButton}) : super(key: key);


  static TextStyle defaultHintStyle = const TextStyle(color: TextColor.black54, fontSize: 15.0,
      fontWeight: FontWeight.normal);
  static TextStyle defaultLabelStyle = const TextStyle(color: TextColor.black54, fontSize: 15.0,
      fontWeight: FontWeight.normal);
  static InputBorder focusenabledDefaultBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide:const BorderSide(color: Color(0xFFFFA726),
    )
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: controller,
        focusNode: focusNode,
        keyboardType: textInputType,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChanged,
        maxLines: maxLines, expands: expands,
        textAlignVertical: TextAlignVertical.top,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          contentPadding:const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          enabledBorder: OutlineInputBorder(borderRadius:BorderRadius.circular(10.0),
          borderSide:const BorderSide(color: Colors.white, width: 3.0)),
          focusedBorder: focusenabledDefaultBorder,
        labelText: labelText,
        hintText: hintText,
        hintStyle: defaultHintStyle,
        labelStyle: defaultLabelStyle,
        )
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        boxShadow: const[
          BoxShadow(
            color: Color(0xFFEEEEEE),
            blurRadius: 15,
            offset: Offset(-1, 2),
          ),
        ],
      ),
    );
  }
}
class PhoneField extends CustomTextField {
  const PhoneField({ Key? key,final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final bool isOutlineBorderTransparent = false,
    final String? hintText = '', final OnChanged? onChanged}) : super(key: key, onFieldSubmitted: onFieldSubmitted, onChanged: onChanged,
      controller: controller, hintText: hintText, focusNode: focusNode,);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: controller,
        focusNode: focusNode,
        keyboardType: TextInputType.phone,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChanged,
        inputFormatters: [
          MaskedInputFormatter('###-###-####'),
          FilteringTextInputFormatter.deny(RegExp(r"[*,#;_+]"))
        ],
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          contentPadding:const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          enabledBorder: OutlineInputBorder(borderRadius:BorderRadius.circular(10.0),
          borderSide:const BorderSide(color: Colors.white, width: 3.0)),
          focusedBorder: CustomTextField.focusenabledDefaultBorder,
          hintText: hintText,
          counterText: '',
        )
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        boxShadow: const[
          BoxShadow(
            color: Color(0xFFEEEEEE),
            blurRadius: 15,
            offset: Offset(-1, 2),
          ),
        ],
      ),
    );
  }
}

class PasswordField extends CustomTextField {
  final bool? isPasswordMatched;
  final IconButton?iconButton;
  final bool obscureText;

  const PasswordField({ Key? key,this.obscureText=true,this.iconButton, final TextEditingController? controller, final OnFieldSubmitted? onFieldSubmitted, final FocusNode? focusNode,
    final String? hintText = '', final OnChanged? onChanged, final bool errorState = false, this.isPasswordMatched = false}) : super(key: key,
    onFieldSubmitted: onFieldSubmitted, onChanged: onChanged, errorState: errorState,
      controller: controller, hintText: hintText, focusNode: focusNode);


static InputBorder enabledDefaultBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: const BorderSide(color: Colors.white)
  );
  static InputBorder customErrorBorder =  OutlineInputBorder(
    borderRadius: BorderRadius.circular(10.0),
    borderSide: const BorderSide(color: Colors.red)
  );
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        obscureText: obscureText,
        controller: controller,
        focusNode: focusNode,
        keyboardType: TextInputType.phone,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChanged,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          contentPadding:const EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: hintText,
          counterText: '',
          enabledBorder: isPasswordMatched! ? PasswordMatchedOutlineBorder.instance.border : errorState ? customErrorBorder
            : enabledDefaultBorder,
          focusedBorder: isPasswordMatched! ? PasswordMatchedOutlineBorder.instance.border : CustomTextField.focusenabledDefaultBorder,
          suffixIcon: iconButton,
        )
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        boxShadow: const[
          BoxShadow(
            color: Color(0xFFEEEEEE),
            blurRadius: 15,
            offset: Offset(-1, 2),
          ),
        ],
      ),
    );
  }
}
class PasswordMatchedOutlineBorder{

  PasswordMatchedOutlineBorder._();

  static PasswordMatchedOutlineBorder? _border;
  static PasswordMatchedOutlineBorder get instance{
    if(_border == null) {
      return _border = PasswordMatchedOutlineBorder._();
    } else {
      return _border!;
    }
  }

  InputBorder get border{
    return OutlineInputBorder(
      borderSide: const BorderSide(color: Color.fromRGBO(109, 208, 168, 1.0)),
      borderRadius: BorderRadius.circular(10.0)
    );
  }
}