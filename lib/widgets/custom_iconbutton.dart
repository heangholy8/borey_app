import 'package:borey_app/style/material_color.dart';
import 'package:flutter/material.dart';

class Iconbuttonback extends StatelessWidget {
  const Iconbuttonback({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 33,
      width: 33,
      child: InkWell(
        child:const Icon(Icons.keyboard_arrow_left,size: 28,color: MatrialColor.black87),
        onTap:(){
          Navigator.of(context).pop();
        } 
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
        boxShadow: const[
          BoxShadow(
            color: Color(0xffe0e0e0),
            blurRadius: 6,
            offset: Offset(2, 2),
          ),
        ],
      ),
    );
  }
}
class Iconbuttonshare extends StatelessWidget {
  VoidCallback?onPressed;
  Iconbuttonshare({ Key? key ,this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 33,
      width: 33,
      child: InkWell(
        child: SizedBox(height: 28,width: 28,child: Image.asset('assets/png/icon_share.png')),
        onTap:onPressed
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
        boxShadow: const[
          BoxShadow(
            color: Color(0xffe0e0e0),
            blurRadius: 6,
            offset: Offset(2, 2),
          ),
        ],
      ),
    );
  }
}