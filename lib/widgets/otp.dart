import 'package:borey_app/style/material_color.dart';
import 'package:borey_app/style/text_color.dart';
import 'package:flutter/material.dart';

class fieldOTP extends StatelessWidget {
  final bool? first,last;
  fieldOTP({ Key? key,this.first,this.last }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      width: 50,
      child: AspectRatio(
        aspectRatio: 1.0,
        child: TextFormField(
          autofocus: true,
          onChanged: (value) {
            if (value.length == 1 && last == false) {
              FocusScope.of(context).nextFocus();
            }
            if (value.isEmpty && first == false) {
              FocusScope.of(context).previousFocus();
            }
          },
          showCursor: false,
          readOnly: false,
          textAlign: TextAlign.center,
          style:const TextStyle(fontSize: 15,color: TextColor.black,),
          keyboardType: TextInputType.number,
          maxLength: 1,
          decoration: InputDecoration(
            contentPadding:const EdgeInsets.only(bottom: 2,left: 2),
            counter:const Offstage(),
            enabledBorder: OutlineInputBorder(
                borderSide:const BorderSide(width: 1.5, color: MatrialColor.grey300),
                borderRadius: BorderRadius.circular(7)),
            focusedBorder: OutlineInputBorder(
                borderSide:const BorderSide(width: 1.5, color: MatrialColor.primaryColor),
                borderRadius: BorderRadius.circular(7)),
          ),
        ),
      ),
    );
  }
}