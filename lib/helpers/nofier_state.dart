enum NotifierState{
  initial,
  loading,
  loaded,
  error,
  empty,
}